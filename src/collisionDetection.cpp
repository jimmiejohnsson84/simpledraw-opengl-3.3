#include <iostream>
#include <memory>
using namespace std;

#include "collisionDetection.h"
#include "shape.h"

void ProjectToAxis(glm::vec3 &normal, std::shared_ptr<Shape> shape, float &min, float &max)
{
	std::vector<glm::vec3> vertices = shape->GetVertices();
	min = glm::dot(normal, vertices[0]);
	max = min;	

	for(auto vertex : vertices)
	{
		double p = glm::dot(normal, vertex);

		if(p < min)
		{
			min = p;
		}
		else if(p > max)
		{
			max = p;
		}
	}
}

bool ShapesIntersect(std::shared_ptr<Shape> a, std::shared_ptr<Shape> b)
{
	std::vector<glm::vec3> bboxA = a->GetBoundingInfo();
	std::vector<glm::vec3> bboxB = b->GetBoundingInfo();

	if(!bboxA.empty() && !bboxB.empty())
	{
		glm::vec3 bboxAMin = bboxA[0];
		glm::vec3 bboxAMax = bboxA[1];

		glm::vec3 bboxBMin = bboxB[0];
		glm::vec3 bboxBMax = bboxB[1];

		if	( 	(bboxAMin.x < bboxBMin.x + (bboxBMax.x - bboxBMin.x ) ) && (bboxAMin.x + (bboxAMax.x - bboxAMin.x) > bboxBMin.x) &&
				(bboxAMin.y < bboxBMin.y + (bboxBMax.y - bboxBMin.y ) ) && (bboxAMin.y + (bboxAMax.y - bboxAMin.y) > bboxBMin.y)
			)
			{
				return ShapesIntersectSAT(a, b);
			}			
		return false;
	}
	else
	{
		return ShapesIntersectSAT(a, b);
	}	
}

/*
	This function uses the SAT method to determine if a collsion has occured.
	This will always work for convex 2D figures.
	SAT - Seperating Axis Theorem is based on the idea that if you can find a seperating axis between two objects - they must not be 
	colliding. To do this, first we need to have the normals for all the faces of the objects.
	Then, we want to project the points of the objects on to an axis (made from the normal of the face) and see if they (points projected on to axis from normal)
	overlap. This can be thought of as shining a light at the object, where the shadow being cast by the shape creating the min/max points.
	We do that for both the objects and if the "shadow" the objects cast are overlapping for all the normals of the objects, then they must be touching.

	This is very computationally heavy, so we will also use a simplifed bounding box test first for the shape Line (which we have) - and only if the bounding box
	tests true will we do the SAT test.
	
*/
bool ShapesIntersectSAT(std::shared_ptr<Shape> a, std::shared_ptr<Shape> b)
{
	vector<glm::vec3> aNormals = a->GetNormals();
	vector<glm::vec3> bNormals = b->GetNormals();

	for(auto aNormal : aNormals)
	{
		float p1Min, p1Max;
		float p2Min, p2Max;
		p1Min = 0.0f;
		p1Max = 0.0f;
		p2Min = 0.0f;
		p2Max = 0.0f;

		ProjectToAxis(aNormal, a, p1Min, p1Max);
		ProjectToAxis(aNormal, b, p2Min, p2Max);

		if( !((p1Min <= p2Max && p1Min >= p2Min) || (p2Min <= p1Max && p2Min >= p1Min)) )
		{			
			return false;
		}
	}

	for(auto bNormal : bNormals)
	{
		float p1Min, p1Max;
		float p2Min, p2Max;
		p1Min = 0.0f;
		p1Max = 0.0f;
		p2Min = 0.0f;
		p2Max = 0.0f;

		ProjectToAxis(bNormal, a, p1Min, p1Max);
		ProjectToAxis(bNormal, b, p2Min, p2Max);

		if( !((p1Min <= p2Max && p1Min >= p2Min) || (p2Min <= p1Max && p2Min >= p1Min)) )
		{			
			return false;
		}
	}	

	return true;
}