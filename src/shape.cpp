#include <iostream>
using namespace std;
#include <vector>
#include <glm/glm.hpp>
#include "shape.h"



Shape::Shape()
{
}
//----------------------------------------------------------------------
Shape::~Shape()
{
}
//----------------------------------------------------------------------
int Shape::CalculateVertexOffset(std::vector<std::shared_ptr<Shape> > shapes)
{
	unsigned int vertexCount = 0;
	for(auto shape : shapes)
	{
		vertexCount += shape->NrVertices();
	}

	return vertexCount * sizeof(glm::vec3);
}
//----------------------------------------------------------------------
int Shape::CalculateVertexColorOffset(std::vector<std::shared_ptr<Shape> > shapes)
{
	unsigned int vertexCount = 0;
	for(auto shape : shapes)
	{
		vertexCount += shape->NrVertices();
	}

	return vertexCount * sizeof(glm::vec3);
}
//----------------------------------------------------------------------
unsigned int  Shape::NrVertices() const
{
	return m_vertices.size();
}
//----------------------------------------------------------------------
int Shape::VertexDataSize() const
{
	return m_vertices.size() * sizeof(glm::vec3);
}
//----------------------------------------------------------------------
int Shape::VertexColorDataSize() const
{
	return m_vertices.size() * sizeof(glm::vec3);
}
//----------------------------------------------------------------------
std::vector<glm::vec3> Shape::GetVertices() const
{
	return m_vertices;
}
//----------------------------------------------------------------------
std::vector<glm::vec3> Shape::GetVerticesColor() const
{
	return m_verticesColor;
}
//----------------------------------------------------------------------
std::vector<glm::vec3> Shape::GetNormals() const
{
	return m_faceNormals;
}
//----------------------------------------------------------------------
void Shape::MarkCollision(bool collision)
{
	m_markedCollision = collision;
}
//----------------------------------------------------------------------
bool Shape::MarkCollision() const
{
	return m_markedCollision;
}

	
