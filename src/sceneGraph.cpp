#include <iostream>
using namespace std;

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include "sceneGraph.h"
#include "shape.h"
#include "initGL.h"
#include "collisionDetection.h"

/*	Maximum number of vertices in a scenegraph is 5000.
	This will be the size of the VBO we create. We could
	of course re-allocate the buffer to let it grow as we need
	but for sake of simplicity, lets make it static
*/	
const unsigned int SceneGraph::maxNumberOfVertices = 5000;

SceneGraph::SceneGraph(GLuint mvp)
{
	m_built = false;
	m_mvp = mvp;
	m_nrVertices = 0;
	m_workingCopyNrVertices = 0;
	m_verticesShapes = 0;
	m_verticesShapesColor = 0;
}
//----------------------------------------------------------------------
SceneGraph::~SceneGraph()
{
	if(m_built)
	{
		glDeleteBuffers(1, &m_verticesShapes);
		glDeleteBuffers(1, &m_verticesShapesColor);
	}
}
//----------------------------------------------------------------------
bool SceneGraph::ShapeCollidesWithShapesInScene(std::shared_ptr<Shape> shape) const
{
	bool collisionDetected = false;
	for(auto shapeToTest : m_shapes)
	{
		if(ShapesIntersect(shape, shapeToTest))
		{
			collisionDetected = true;
		}
	}
	return collisionDetected;
}
//----------------------------------------------------------------------
bool SceneGraph::AddShape(std::shared_ptr<Shape> shape)
{
	bool retValue = false;
	if(m_nrVertices + shape->NrVertices() < maxNumberOfVertices )
	{
		bool collisionDetected = ShapeCollidesWithShapesInScene(shape);
		if(!collisionDetected)
		{
			AddToVBO(shape);
			m_shapes.push_back(shape);
		}

		retValue = !collisionDetected;
	}
	
	return retValue;
}
//----------------------------------------------------------------------
void SceneGraph::BuildVBOS()
{
	glGenBuffers(1, &m_verticesShapes);
	glGenBuffers(1, &m_verticesShapesColor);		

	glBindBuffer(GL_ARRAY_BUFFER, m_verticesShapes);
	glBufferData(GL_ARRAY_BUFFER, maxNumberOfVertices * sizeof(glm::vec3), NULL, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, m_verticesShapesColor);
	glBufferData(GL_ARRAY_BUFFER, maxNumberOfVertices * sizeof(glm::vec3), NULL, GL_STATIC_DRAW);		

	m_built = true;
}
//----------------------------------------------------------------------
void SceneGraph::AddToVBO(std::shared_ptr<Shape> shape)
{
	if(!m_built)
	{
		BuildVBOS();
	}

	// Find the offset into the VBO where we want to add the new vertices
	int vertexOffset = Shape::CalculateVertexOffset(m_shapes);
	int vertexDataSize = shape->VertexDataSize();
	std::vector<glm::vec3> vertices = shape->GetVertices();
	glBindBuffer(GL_ARRAY_BUFFER, m_verticesShapes);
	glBufferSubData(GL_ARRAY_BUFFER, vertexOffset, vertexDataSize, &vertices[0]);

	// Each vertex has a color attached to it, which goes into the other VBO. Same principle here, find the offset and add.
	int colorOffset = Shape::CalculateVertexColorOffset(m_shapes);
	int colorDataSize = shape->VertexColorDataSize();
	std::vector<glm::vec3> verticesColor = shape->GetVerticesColor();
	glBindBuffer(GL_ARRAY_BUFFER, m_verticesShapesColor);
	glBufferSubData(GL_ARRAY_BUFFER, colorOffset, colorDataSize, &verticesColor[0]);

	m_nrVertices += shape->NrVertices();
}
//----------------------------------------------------------------------
void SceneGraph::ShapeWorkingCopy(std::shared_ptr<Shape> shape)
{
	if(!m_built)
	{
		BuildVBOS();
	}

	int vertexOffset = m_nrVertices * sizeof(glm::vec3);
	int vertexDataSize = shape->VertexDataSize();
	std::vector<glm::vec3> vertices = shape->GetVertices();
	m_workingCopyNrVertices = shape->NrVertices();
	glBindBuffer(GL_ARRAY_BUFFER, m_verticesShapes);
	glBufferSubData(GL_ARRAY_BUFFER, vertexOffset, vertexDataSize, &vertices[0]);

	int colorOffset = m_nrVertices * sizeof(glm::vec3);
	int colorDataSize = shape->VertexColorDataSize();
	std::vector<glm::vec3> verticesColor = shape->GetVerticesColor();
	glBindBuffer(GL_ARRAY_BUFFER, m_verticesShapesColor);
	glBufferSubData(GL_ARRAY_BUFFER, colorOffset, colorDataSize, &verticesColor[0]);

	// Go through all the shapes in the scene and check if working copy shape intersects any of them.
	// If so, mark them so we can update the color of them to red in the rendering function
	for(auto shapeToTest : m_shapes)
	{
		if(ShapesIntersect(shape, shapeToTest))
		{
			shapeToTest->MarkCollision(true);
		}
		else
		{
			shapeToTest->MarkCollision(false);
		}
	}
}
//----------------------------------------------------------------------
void SceneGraph::RemoveWorkingCopy()
{
	ResetWorkingCopy();
	ResetShapesCollision();
}
//----------------------------------------------------------------------
void SceneGraph::ResetWorkingCopy()
{
	m_workingCopyNrVertices = 0;	
}
//----------------------------------------------------------------------
void SceneGraph::ResetShapesCollision()
{
	unsigned int vertexCount = 0;
	for(auto shape : m_shapes)
	{
		if(shape->MarkCollision())
		{
			int colorOffset = vertexCount * sizeof(glm::vec3);
			int colorDataSize = shape->VertexColorDataSize();
			std::vector<glm::vec3> verticesColor = shape->GetVerticesColor();
			glBindBuffer(GL_ARRAY_BUFFER, m_verticesShapesColor);
			glBufferSubData(GL_ARRAY_BUFFER, colorOffset, colorDataSize, &verticesColor[0]);
		}

		shape->MarkCollision(false);
		vertexCount += shape->NrVertices();
	}
}
//----------------------------------------------------------------------
void SceneGraph::MarkCollidedShapesRed()
{
	unsigned int vertexCount = 0;
	for(auto shape : m_shapes)
	{
		if(shape->MarkCollision())
		{
			int colorOffset = vertexCount * sizeof(glm::vec3);
			int colorDataSize = shape->VertexColorDataSize();
			std::vector<glm::vec3> verticesColor;
			for(int i = 0; i < shape->NrVertices(); i++)
			{
				verticesColor.push_back(glm::vec3(1.0f, 0.0f, 0.0f));
			}
			glBindBuffer(GL_ARRAY_BUFFER, m_verticesShapesColor);
			glBufferSubData(GL_ARRAY_BUFFER, colorOffset, colorDataSize, &verticesColor[0]);			
		}
		vertexCount += shape->NrVertices();
	}

}
//----------------------------------------------------------------------
void SceneGraph::RenderGraph()
{
	// If the graph contains something to display, render it
	if(m_built)
	{		
		/*
			Create a orthographic projection. This is convenient since we are working in 2D. We will now have a canvas
			the size of (g_WindowWidth, g_WindowHeight) and we just ignore the Z axis.
		*/
  		glm::mat4 mvp = glm::ortho(0.0f, static_cast<float>(g_WindowWidth), static_cast<float>(g_WindowHeight), 0.0f);
		glUniformMatrix4fv(m_mvp, 1, GL_FALSE, &mvp[0][0]);			

		// Make all the shapes marked as collided with the working copy shape as red in the scene
		MarkCollidedShapesRed();

		// Enable our 2 vertex attribute arrays, first one contaning positional data and the second contaning color data
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, m_verticesShapes);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, m_verticesShapesColor);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

		// Draw all the arrays that we have copied from m_shapes + the current "working" shape the user is drawing.
		glDrawArrays(GL_TRIANGLES, 0, m_nrVertices + m_workingCopyNrVertices);
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);

		// Reset the working copy, allowing it to be updated again in next frame by using ShapeWorkingCopy.
		// Also reset all the collision indications we had, returning the shapes to their original color
		ResetWorkingCopy();
		ResetShapesCollision();
	}	
}