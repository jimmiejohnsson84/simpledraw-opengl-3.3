#include <iostream>
using namespace std;

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "handleInput.h"
#include "sceneGraph.h"
#include "shape.h"
#include "line.h"
#include "triangle.h"

InputState::InputState()
{
	m_pressedLMouseButton = false;
	m_workingCopy = NULL;
	m_workingCopyTriangle = NULL;
	m_lineWidth = 1.0f;

	m_pressedSpaceButtonFirstTime = false;
	m_pressedSpaceButtonLastTime = 0;	
    
	m_pressedTabButtonLastTime = false;
    m_pressedTabButtonFirstTime = 0;

	m_drawingLines = true;	

    m_currentColor = 0;
	m_colorPalette.push_back(glm::vec3(0.0f, 0.0f, 0.0f)); // Black
	m_colorPalette.push_back(glm::vec3(0.0f, 1.0f, 0.0f)); // Green
	m_colorPalette.push_back(glm::vec3(0.0f, 0.0f, 1.0f)); // Blue
}
//----------------------------------------------------------------------
InputState::~InputState()
{
}
//----------------------------------------------------------------------
bool InputState:: pressedLMouseButton() const
{
	return m_pressedLMouseButton;
}
//----------------------------------------------------------------------
void InputState::pressedLMouseButton(bool pressed)
{
	m_pressedLMouseButton = pressed;
}
//----------------------------------------------------------------------
double InputState::PressedSpaceButtonLastTime() const
{
	return m_pressedSpaceButtonLastTime;
}
//----------------------------------------------------------------------
void InputState::PressedSpaceButtonLastTime(double lastTime)
{
	m_pressedSpaceButtonLastTime = lastTime;
}
//----------------------------------------------------------------------
bool InputState::PressedSpaceButtonFirstTime() const
{
	return m_pressedSpaceButtonFirstTime;
}
//----------------------------------------------------------------------
void InputState::PressedSpaceButtonFirstTime(bool pressedSpaceButtonFirstTime)
{
	m_pressedSpaceButtonFirstTime = pressedSpaceButtonFirstTime;
}
//----------------------------------------------------------------------
bool InputState::CanSwitchColor(double currentTime) const
{
	return (!m_pressedSpaceButtonFirstTime || float(currentTime - m_pressedSpaceButtonLastTime) > 0.5f);
}
//----------------------------------------------------------------------
void InputState::SwitchColor()
{
	m_currentColor++;
	if(m_currentColor >= 3)
	{
		m_currentColor = 0;
	}
}
//----------------------------------------------------------------------
double InputState::PressedTabButtonLastTime() const
{
	return m_pressedTabButtonLastTime;
}
//----------------------------------------------------------------------
void InputState::PressedTabButtonLastTime(double lastTime)
{
	m_pressedTabButtonLastTime = lastTime;
}
//----------------------------------------------------------------------
bool InputState::PressedTabButtonFirstTime() const
{
	return m_pressedTabButtonFirstTime;
}
//----------------------------------------------------------------------
void InputState::PressedTabButtonFirstTime(bool pressedTabButtonFirstTime)
{
	m_pressedTabButtonFirstTime = pressedTabButtonFirstTime;
}
//----------------------------------------------------------------------
bool InputState::CanSwitchShape(double currentTime) const
{
	return (!m_pressedTabButtonFirstTime || float(currentTime - m_pressedTabButtonLastTime) > 0.5f);
}
//----------------------------------------------------------------------
void InputState::SwitchShape()
{
	m_drawingLines = !m_drawingLines;
}
//----------------------------------------------------------------------
bool InputState::DrawLines() const
{
	return m_drawingLines;
}
//----------------------------------------------------------------------
glm::vec3 InputState::CurrentColor() const
{
	return m_colorPalette[m_currentColor];
}
//----------------------------------------------------------------------
void InputState::LineWidthIncrease()
{
	if(m_lineWidth < 7.0f)
	{
		m_lineWidth += 0.5f;
	}
}
//----------------------------------------------------------------------	
void InputState::LineWidthDecrease()
{
	if(m_lineWidth > 1.0f)
	{
		m_lineWidth -= 0.5f;
	}
}
//----------------------------------------------------------------------
float InputState::LineWidth() const
{
	return m_lineWidth;
}
//----------------------------------------------------------------------
void InputState::StartDrawingLine(double xpos, double ypos, SceneGraph &sceneGraph)
{
	// Create a working copy from (mouseX, mouseY) to (mouseX + 1, mouseY + 1). We want it to have a length > 0
	// when we create it so that we can see if the line created is inside an already existing line in the graph
	m_workingCopy = make_shared<Line>(glm::vec3(xpos, ypos, 0.0f), glm::vec3(xpos + 1.0f, ypos + 1.0f, 0.0f), CurrentColor(), 1.0f);

	// Dont allow user to start drawing on an existing shape
	if(sceneGraph.ShapeCollidesWithShapesInScene(m_workingCopy))
	{
		m_workingCopy = NULL;
	}	
}
//----------------------------------------------------------------------
void InputState::StartDrawingTriangle(double xpos, double ypos, SceneGraph &sceneGraph)
{
	m_workingCopyTriangle = make_shared<Triangle>(glm::vec3(xpos, ypos, 0.0f), 1.0f, CurrentColor());
	m_mouseButtonLeftLast = glm::vec3(xpos, ypos, 0.0f);			

	// Dont allow user to start drawing on an existing shape
	if(sceneGraph.ShapeCollidesWithShapesInScene(m_workingCopyTriangle))
	{
		m_workingCopyTriangle = NULL;
	}	
}
//----------------------------------------------------------------------
void InputState::HandleKeyInput(GLFWwindow* window, SceneGraph &sceneGraph, double &lastTime)
{
	// Time difference between frames
	double currentTime = glfwGetTime();
	float deltaTime = float(currentTime - lastTime);

	glfwPollEvents();

	// Mouse input
	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);

	if(glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS)
	{
		pressedLMouseButton(true);
	}

	// Left mouse button -> Create new shape
	if(pressedLMouseButton() && glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_RELEASE)
	{
		pressedLMouseButton(false);
		bool startDrawing = DrawLines() ? m_workingCopy == NULL : m_workingCopyTriangle == NULL;
		if(startDrawing)
		{
			if(DrawLines())
			{
				StartDrawingLine(xpos, ypos, sceneGraph);
			}
			else
			{
				StartDrawingTriangle(xpos, ypos, sceneGraph);
			}
		}
		else
		{
			if(DrawLines())
			{
				if(sceneGraph.AddShape(m_workingCopy))
				{
					sceneGraph.RemoveWorkingCopy();
					m_workingCopy = NULL;
				}
			}
			else
			{
				if(sceneGraph.AddShape(m_workingCopyTriangle))
				{
					sceneGraph.RemoveWorkingCopy();
					m_workingCopyTriangle = NULL;
				}		
			}
		}
	}

	bool drawing = DrawLines() ? m_workingCopy != NULL : m_workingCopyTriangle != NULL;

	// Key input
	// Space -> Change color
	if (glfwGetKey(window, GLFW_KEY_SPACE ) == GLFW_PRESS 
		&& CanSwitchColor(currentTime))
	{
		SwitchColor();
		PressedSpaceButtonFirstTime(true);
		PressedSpaceButtonLastTime(currentTime);
	}

	// Tab -> Switch between line/triangle shape
	if(!drawing)
	{
		if (glfwGetKey(window, GLFW_KEY_TAB ) == GLFW_PRESS 
			&& CanSwitchShape(currentTime))
		{
			SwitchShape();
			PressedTabButtonFirstTime(true);
			PressedTabButtonLastTime(currentTime);
		}	
	}

	// +/- = Change line width
	if(glfwGetKey(window, GLFW_KEY_KP_ADD) == GLFW_PRESS)
	{
		LineWidthIncrease();
	}
	else if(glfwGetKey(window, GLFW_KEY_KP_SUBTRACT) == GLFW_PRESS)
	{
		LineWidthDecrease();
	}	

	if(drawing)
	{
		if(DrawLines())
		{
			m_workingCopy->SetColor(CurrentColor());
			m_workingCopy->SetEndPosition(glm::vec3(xpos, ypos, 0.0f));
			m_workingCopy->LineWidth(LineWidth());
			sceneGraph.ShapeWorkingCopy(m_workingCopy);				
		}
		else
		{
			m_workingCopyTriangle->Color(CurrentColor());

			// Calculate rotation & size
			if(abs(m_mouseButtonLeftLast.x - xpos) >= 1.0f || abs(m_mouseButtonLeftLast.y - ypos) >= 1.0f)
			{
				glm::vec3 lastMousePosToCurrent =  glm::vec3(xpos, ypos, 0.0f) - m_mouseButtonLeftLast;
				glm::vec3 lastMousePosToCurrentNorm = glm::normalize(lastMousePosToCurrent);
				glm::vec3 dirUp(0.0f, -1.0f, 0.0f);
				float angle = acos(glm::dot(lastMousePosToCurrentNorm, dirUp));
				if(lastMousePosToCurrent.x < 0)
				{
					angle = glm::radians(360.0f) - angle;
				}
				m_workingCopyTriangle->Rotation(angle);	

				float size = sqrtf(lastMousePosToCurrent.x * lastMousePosToCurrent.x + lastMousePosToCurrent.y * lastMousePosToCurrent.y);
				m_workingCopyTriangle->Size(size);
				sceneGraph.ShapeWorkingCopy(m_workingCopyTriangle);
			}				
		}		
	}

	lastTime = currentTime;
}