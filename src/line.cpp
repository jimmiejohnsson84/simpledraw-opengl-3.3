#include <iostream>
using namespace std;

#include <glm/glm.hpp>
#include "line.h"


Line::Line(glm::vec3 start, glm::vec3 end, glm::vec3 color, float lineWidth)
{
	m_lineWidth = lineWidth;
	m_start = start;

	CreateLinePositionAndNormalData(end);
	CreateLineColorData(color);
	CreateLineBoundingBoxData();
}
//----------------------------------------------------------------------
void Line::CreateLinePositionAndNormalData(glm::vec3 end)
{
	/*
		To make the line have thickness, we actually create two triangles making a quad betweeen the start and endpoint.
		It will be centered around the start and end point.

		First, we need to find the line perpendicular to this one. This can be found using a two dimensional rotation matrix
		[0   1] [ x ]  =  [  y ]
		[-1  0] [ y ]  =  [ -x ]

		Then we normalize the perpendicular line and use it to calculate two points that puts x,y in the middle.
		We do the same thing for the end point of the line.
		After that we create 2 triangles from these points which is what will be rendered out later
	*/
	glm::vec3 lineDir = end - m_start;
	glm::vec3 perpendicularLine(lineDir.y, -lineDir.x, 0.0f);
	perpendicularLine = glm::normalize(perpendicularLine);
	
	glm::vec3 startP1 = m_start + (perpendicularLine * m_lineWidth);
	glm::vec3 startP2 = m_start + perpendicularLine * (-m_lineWidth);

	glm::vec3 endP1 = end + (perpendicularLine * m_lineWidth);
	glm::vec3 endP2 = end + perpendicularLine * (-m_lineWidth);

	m_vertices.clear();
	m_vertices.push_back(startP2);
	m_vertices.push_back(endP2);
	m_vertices.push_back(endP1);

	m_vertices.push_back(startP1);
	m_vertices.push_back(startP2);
	m_vertices.push_back(endP1);

	CreateLineNormalData(startP1, startP2, endP1);
}
//----------------------------------------------------------------------
void Line::CreateLineColorData(glm::vec3 color)
{
	m_verticesColor.clear();
	for(int i = 0; i < m_vertices.size(); i++)
	{
		m_verticesColor.push_back(color);
	}	
}
//----------------------------------------------------------------------
void Line::CreateLineNormalData(glm::vec3 startP1, glm::vec3 startP2, glm::vec3 endP1)
{
	// Calculate normals for collision detection
	glm::vec3 n1 = glm::normalize(endP1 - startP1);
	glm::vec3 n2 = -n1;
	glm::vec3 n3 = glm::normalize(startP2 - startP1);
	glm::vec3 n4 = -n3;

	m_faceNormals.clear();
	m_faceNormals.push_back(n1);
	m_faceNormals.push_back(n2);
	m_faceNormals.push_back(n3);
	m_faceNormals.push_back(n4);	
}
//----------------------------------------------------------------------
void Line::CreateLineBoundingBoxData()
{
	// Find biggest & smallest (X, Y)
	float minX = m_vertices[0].x;
	float minY = m_vertices[0].y;

	float maxX = m_vertices[0].x;
	float maxY = m_vertices[0].y;
	
	for(auto vertex : m_vertices)
	{
		if(vertex.x < minX)
		{
			minX = vertex.x;
		}
		if(vertex.x > maxX)
		{
			maxX = vertex.x;
		}

		if(vertex.y < minY)
		{
			minY = vertex.y;
		}
		if(vertex.y > maxY)
		{
			maxY = vertex.y;
		}		
	}
	m_boundingBox.clear();
	m_boundingBox.push_back(glm::vec3(minX, minY, 0.0f));
	m_boundingBox.push_back(glm::vec3(maxX, maxY, 0.0f));
}
//----------------------------------------------------------------------
void Line::SetColor(glm::vec3 color)
{
	CreateLineColorData(color);
}
//----------------------------------------------------------------------
void Line::SetEndPosition(glm::vec3 end)
{
	CreateLinePositionAndNormalData(end);
	CreateLineBoundingBoxData();
}
//----------------------------------------------------------------------
void Line::LineWidth(float lineWidth)
{
	m_lineWidth = lineWidth;
}
//----------------------------------------------------------------------	
float Line::LineWidth() const
{
	return m_lineWidth;
}
//----------------------------------------------------------------------
std::vector<glm::vec3>Line::GetBoundingInfo() const
{
	return m_boundingBox;
}
//----------------------------------------------------------------------
Line::~Line()
{
}