#ifndef HANDLEINPUT_H
#define HANDLEINPUT_H

#include <glm/glm.hpp>
#include <vector>
#include <memory>

// Forward declarations
class SceneGraph;
class Line;
class Triangle;

class InputState
{
    public:
    InputState();
    ~InputState();

    void        HandleKeyInput(GLFWwindow* window, SceneGraph &sceneGraph, double &lastTime);

    private:

    bool        pressedLMouseButton() const;
    void        pressedLMouseButton(bool pressed);

    double      PressedSpaceButtonLastTime() const;
    void        PressedSpaceButtonLastTime(double lastTime);

    bool        PressedSpaceButtonFirstTime() const;
    void        PressedSpaceButtonFirstTime(bool pressedSpaceButtonFirstTime);   

    double      PressedTabButtonLastTime() const;
    void        PressedTabButtonLastTime(double lastTime);

    bool        PressedTabButtonFirstTime() const;
    void        PressedTabButtonFirstTime(bool pressedTabButtonFirstTime);   

    bool        CanSwitchColor(double currentTime) const;
    void        SwitchColor();
    glm::vec3   CurrentColor() const;
    
    bool        CanSwitchShape(double currentTime) const;
    void        SwitchShape();
    bool        DrawLines() const;

    float       LineWidth() const;
    void        LineWidthIncrease();
    void        LineWidthDecrease();   

    void        StartDrawingLine(double xpos, double ypos, SceneGraph &sceneGraph);
    void        StartDrawingTriangle(double xpos, double ypos, SceneGraph &sceneGraph);

    bool                        m_pressedLMouseButton;
    float                       m_lineWidth;

    double                      m_pressedSpaceButtonLastTime;
    bool                        m_pressedSpaceButtonFirstTime;

    double                      m_pressedTabButtonLastTime;
    bool                        m_pressedTabButtonFirstTime;
    
    std::vector<glm::vec3>      m_colorPalette;
    unsigned int                m_currentColor;    

    std::shared_ptr<Line>       m_workingCopy;
    std::shared_ptr<Triangle>   m_workingCopyTriangle;

    bool                        m_drawingLines;

    glm::vec3                   m_mouseButtonLeftLast;
};

#endif