#ifndef INITGL_H
#define INITGL_H

// Forward declarations
struct GLFWwindow;

const unsigned int g_WindowWidth = 1024;
const unsigned int g_WindowHeight = 768;


int InitGL(GLFWwindow** window);

#endif