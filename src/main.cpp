#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <memory>
using namespace std;

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

#include "shader.h"
#include "initGL.h"
#include "handleInput.h"
#include "sceneGraph.h"
#include "line.h"

void DisplayStartupText()
{
	cout << "Simple Draw - A simple drawing and collision detection program" << endl;
	cout << "This program lets you draw lines and triangles." << endl;
	cout << "The lines and triangles may not intersect eachother. This is indicated by \nthe existing shapes that intersect the new one turning red." << endl;
	cout << "*** CONTROLS ***" << endl;
	cout << "Left mouse button - start drawing a line/triangle. Press again, finish  \ndrawing line/triangle." << endl;
	cout << "+/- Increase/decrease line width." << endl;
	cout << "Space - switch color." << endl;
	cout << "Tab - Switch between drawing lines/triangles ." << endl;
	cout << "Esc - Exit" << endl;
	cout << "*** CONTROLS ***" << endl;
	cout << "Press enter to continue ";
}

int main( void )
{
	DisplayStartupText();
	getchar(); // Wait for user to press Enter
	
	GLFWwindow* window;

	if(InitGL(&window) == -1)
	{
		return -1;
	}

	GLuint vertexArrayID;
	glGenVertexArrays(1, &vertexArrayID);
	glBindVertexArray(vertexArrayID);

	// Create and compile our GLSL program from the shaders
	GLuint programID = LoadShaders( "connectLines.vertexshader", "connectLines.fragmentshader" );	

	// Get handles for data in the shader
	GLuint mvp = glGetUniformLocation(programID, "MVP");

	// Set background color
	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);		

	double lastTime = glfwGetTime();

	SceneGraph sceneGraph(mvp);
	InputState inputState;
	
	// Use our shader
	glUseProgram(programID);
	while(glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS && glfwWindowShouldClose(window) == 0)
	{
		glClear(GL_COLOR_BUFFER_BIT);

		inputState.HandleKeyInput(window, sceneGraph, lastTime);
		sceneGraph.RenderGraph();

		glfwPollEvents();
		glfwSwapBuffers(window);		
	}

	// Clean up and close OpenGL
	glDeleteVertexArrays(1, &vertexArrayID);	
	glfwTerminate();

	return 0;
}

