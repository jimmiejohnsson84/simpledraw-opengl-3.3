#ifndef SHADER_H
#define SHADER_H

/* 
    This code was provided by opengl-tutorials (http://www.opengl-tutorial.org/)
    It will load and create the vertex and fragment shader for us
*/
GLuint LoadShaders(const char * vertex_file_path,const char * fragment_file_path);

#endif
