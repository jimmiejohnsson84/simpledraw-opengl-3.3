#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "shape.h"

/*
	Triangle.
	The triangle can have different size and rotation.
	
	There is no bounding box created for the triangle - but one can easily create one.

*/
class Triangle : public Shape
{
	public:

	Triangle(glm::vec3 center, float size, glm::vec3 color);
	~Triangle();

	void						Color(glm::vec3 color);
	void						Size(float size);
	float						Size() const;

	void						Rotation(float rotation);

	virtual std::vector<glm::vec3>		GetBoundingInfo() const;

	private:
	
	void	CreateTrianglePositionAndNormalData();
	void	CreateTriangleColorData(glm::vec3 color);
	void	CreateTriangleNormalData();

	float		m_rotation;
	float		m_size;
	glm::vec3	m_center;
};

#endif