#ifndef SCENEGRAPH_H
#define SCENEGRAPH_H

// Forward declarations
class Shape;

#include <memory>
#include <vector>

/*
	SceneGraph - a simple scene graph with an array of shapes that can be in the scene.
	This scene graph provides a mechanism for interactively drawing a new shape - ShapeWorkingCopy.
	Pass a shape here that is currently being drawn by the user and it will be inserted in the scene and then removed again after calling
	RenderGraph.

	The scene graph is built on the assumption that all shape objects are represented as triangles. Since the triangle is the simplest convex polygon
	it is usually easy to build more complex polygons on it and that is the reason that the scene graph was built around it.

*/
class SceneGraph
{
	public:
	SceneGraph(GLuint mvp);
	~SceneGraph();

	void RenderGraph();
	bool ShapeCollidesWithShapesInScene(std::shared_ptr<Shape> shape) const;	
	bool AddShape(std::shared_ptr<Shape> shape);
	void ShapeWorkingCopy(std::shared_ptr<Shape> shape);

	void RemoveWorkingCopy();

	private:

	void BuildVBOS();
	void AddToVBO(std::shared_ptr<Shape> shape);
	
	void ResetWorkingCopy();
	void ResetShapesCollision();

	void MarkCollidedShapesRed();

	// The shapes in the graph
	std::vector<std::shared_ptr<Shape> > m_shapes;

	// VBO for rendering
	GLuint	m_verticesShapes;
	GLuint	m_verticesShapesColor;

	// True if a VBO has been built
	bool m_built;

	// Lets limit ourselves to a predefined number of vertices
	static const unsigned int maxNumberOfVertices;

	unsigned int m_nrVertices;

	// Offset to working copy is always m_nrVertices
	unsigned int m_workingCopyNrVertices;

	GLuint m_mvp;
};

#endif