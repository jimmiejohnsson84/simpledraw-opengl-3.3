#ifndef SHAPE_H
#define SHAPE_H

#include <vector>
#include <memory>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

/*
	Shape - Abstract data type for shapes.
	
	Provides a common interface used by the SceneGraph and the collision detection code.

	To create an instansiable class, you must provide an implementation of GetBoundingInfo().
	This function can return an empty vector, if you have not written a function that creates a bounding box for your shape.
	If so, your shape will always be SAT tested against other shapes - otherwise, the bounding box created will be used first to determine
	collision.

	Inherited classes should provide functionallity for creaing the positional, color and normal data as well.
*/
class Shape
{
	public:

	Shape();
	~Shape();

	static int 				CalculateVertexOffset(std::vector<std::shared_ptr<Shape> > shapes);	
	static int				CalculateVertexColorOffset(std::vector<std::shared_ptr<Shape> > shapes);

	// Needed to load Shape into VBO
	unsigned int 			NrVertices() const;
	int 					VertexDataSize() const;
	int 					VertexColorDataSize() const;
	
	std::vector<glm::vec3>		GetVertices() const;
	std::vector<glm::vec3>		GetVerticesColor() const;
	std::vector<glm::vec3>		GetNormals() const;
	void						MarkCollision(bool collision);
	bool						MarkCollision() const;

	// Pure virtual functions - you must provide an implementation for these in your dervied class
	virtual std::vector<glm::vec3>		GetBoundingInfo() const = 0;	

	protected:
	std::vector<glm::vec3>		m_vertices;
	std::vector<glm::vec3>		m_verticesColor;
	std::vector<glm::vec3>		m_faceNormals;

	bool						m_markedCollision;
};

#endif