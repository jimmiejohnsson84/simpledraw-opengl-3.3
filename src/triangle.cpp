#include <iostream>
using namespace std;

#include <glm/glm.hpp>
#include "triangle.h"

Triangle::Triangle(glm::vec3 center, float size, glm::vec3 color)
{
	m_rotation = 0.0f;
	m_center = center;
	m_size = size;

	CreateTrianglePositionAndNormalData();
	CreateTriangleColorData(color);
}
//----------------------------------------------------------------------
void Triangle::CreateTrianglePositionAndNormalData()
{
	// Build a triangle centered around (0,0)
	glm::vec4 top(0.0f, -m_size / 2.0f , 0.0f, 1.0f);
	glm::vec4 bottomLeft(-m_size / 2.0f, m_size / 2.0f , 0.0f, 1.0f);
	glm::vec4 bottomRight(m_size / 2.0f, m_size / 2.0f , 0.0f, 1.0f);

	// Create a rotational matrix around the Z axis and rotate it with m_rotation.
	glm::mat4 model = glm::mat4(1.0f);
	model = glm::rotate(model, m_rotation, glm::vec3(0.0f, 0.0f, 1.0f));

	// Apply rotation to the triangle we built
	top = model * top;
	bottomLeft = model * bottomLeft;
	bottomRight = model * bottomRight;

	// Translate it to the position we clicked
	glm::vec3 topPos = glm::vec3(m_center.x + top.x, m_center.y + top.y, 0.0f);
	glm::vec3 bottomLeftPos = glm::vec3(m_center.x + bottomLeft.x, m_center.y + bottomLeft.y, 0.0f);
	glm::vec3 bottomRightPos = glm::vec3(m_center.x + bottomRight.x, m_center.y + bottomRight.y, 0.0f);
	
	m_vertices.clear();
	m_vertices.push_back(topPos);
	m_vertices.push_back(bottomRightPos);
	m_vertices.push_back(bottomLeftPos);

	CreateTriangleNormalData();
}
//----------------------------------------------------------------------
void Triangle::CreateTriangleColorData(glm::vec3 color)
{
	m_verticesColor.clear();
	for(int i = 0; i < m_vertices.size(); i++)
	{
		m_verticesColor.push_back(color);
	}	
}
//----------------------------------------------------------------------
void Triangle::CreateTriangleNormalData()
{
	glm::vec3 top_bottomRight = m_vertices[1] - m_vertices[0];
	glm::vec3 bottomLeft_bottomRight = m_vertices[2] - m_vertices[1];
	glm::vec3 bottomLeft_top = m_vertices[2] - m_vertices[0];

	top_bottomRight = glm::normalize(top_bottomRight);
	bottomLeft_bottomRight = glm::normalize(bottomLeft_bottomRight);
	bottomLeft_top = glm::normalize(bottomLeft_top);

	/*
		Create normals from a line perpendicular to each side
	*/
	m_faceNormals.clear();
	m_faceNormals.push_back(glm::vec3(top_bottomRight.y, -top_bottomRight.x, 0.0f));
	m_faceNormals.push_back(glm::vec3(bottomLeft_bottomRight.y, -bottomLeft_bottomRight.x, 0.0f));
	m_faceNormals.push_back(glm::vec3(bottomLeft_top.y, -bottomLeft_top.x, 0.0f));	
}
//----------------------------------------------------------------------
void Triangle::Color(glm::vec3 color)
{
	CreateTriangleColorData(color);
}
//----------------------------------------------------------------------
void Triangle::Size(float size)
{
	m_size = size;
	CreateTrianglePositionAndNormalData();
}
//----------------------------------------------------------------------
void Triangle::Rotation(float rotation)
{
	m_rotation = rotation;
}
//----------------------------------------------------------------------
float Triangle::Size() const
{
	return m_size;
}
//----------------------------------------------------------------------	
std::vector<glm::vec3> Triangle::GetBoundingInfo() const
{
	return std::vector<glm::vec3>();
}
//----------------------------------------------------------------------	
Triangle::~Triangle()
{
}