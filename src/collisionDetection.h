#ifndef COLLISIONDETECTION_H
#define COLLISIONDETECTION_H

#include <memory>
#include <glm/glm.hpp>

// Forward declarations
class Shape;

// First with simple bbox test (if possible) and then using SAT
bool ShapesIntersect(std::shared_ptr<Shape> a, std::shared_ptr<Shape> b);

// SAT collision detection test
bool ShapesIntersectSAT(std::shared_ptr<Shape> a, std::shared_ptr<Shape> b);


#endif