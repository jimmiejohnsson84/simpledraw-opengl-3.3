#ifndef LINE_H
#define LINE_H

#include "shape.h"

/*
	Line - A line.
	
	The line is graphically represented as a rotated quad, consisting of 2 triangles.
	This is to provide a means of adding line thickness. The old way of adding line width using "glLineWidth" is deprecated,
	so it makes more sense to give lines width this way.

	The line supports having different width and color, using LineWidth and SetColor.
*/
class Line : public Shape
{
	public:

	Line(glm::vec3 start, glm::vec3 end, glm::vec3 color, float lineWidth);
	~Line();

	void								SetColor(glm::vec3 color);
	void								SetEndPosition(glm::vec3 end);
	void								LineWidth(float lineWidth);
	float								LineWidth() const;
	virtual std::vector<glm::vec3>		GetBoundingInfo() const;

	private:

	void	CreateLinePositionAndNormalData(glm::vec3 end);
	void	CreateLineColorData(glm::vec3 color);
	void	CreateLineNormalData(glm::vec3 startP1,glm::vec3 startP2, glm::vec3 endP1);
	void	CreateLineBoundingBoxData();

	glm::vec3 m_start;
	std::vector<glm::vec3> m_boundingBox;
	float m_lineWidth;
};

#endif