# CMake entry point
cmake_minimum_required (VERSION 3.0)
project (Excavator)

find_package(OpenGL REQUIRED)

if( CMAKE_BINARY_DIR STREQUAL CMAKE_SOURCE_DIR )
    message( FATAL_ERROR "Please select another Build Directory" )
endif()
if( CMAKE_SOURCE_DIR MATCHES " " )
	message( "Your Source Directory contains spaces. If you experience problems when compiling, this can be the cause." )
endif()
if( CMAKE_BINARY_DIR MATCHES " " )
	message( "Your Build Directory contains spaces. If you experience problems when compiling, this can be the cause." )
endif()

# Compile external dependencies
add_subdirectory (external)

if(INCLUDE_DISTRIB)
	add_subdirectory(distrib)
endif(INCLUDE_DISTRIB)

include_directories(
	external/glfw-3.1.2/include/
	external/glm-0.9.7.1/
	external/glew-1.13.0/include/
    ${OPENGL_INCLUDE_DIRS}
	.
)

set(ALL_LIBS
	${OPENGL_LIBRARY}
	glfw
	GLEW_1130
)

add_definitions(
	-DTW_STATIC
	-DTW_NO_LIB_PRAGMA
	-DTW_NO_DIRECT3D
	-DGLEW_STATIC
	-D_CRT_SECURE_NO_WARNINGS
    -std=c++11
)

# simpleDraw
add_executable(simpleDraw 
	src/main.cpp
    src/initGL.cpp
    src/initGL.h    
    src/handleInput.cpp
    src/handleInput.h
    src/sceneGraph.cpp
    src/sceneGraph.h
    src/shape.cpp
    src/shape.h
    src/line.cpp
    src/line.h
    src/triangle.cpp
    src/triangle.h	
    src/collisionDetection.cpp
    src/collisionDetection.h
    
    src/shader.cpp
	src/shader.h
       
	src/connectLines.fragmentshader
	src/connectLines.vertexshader
)
target_link_libraries(simpleDraw
	${ALL_LIBS}
)
add_custom_command(
   TARGET simpleDraw POST_BUILD
   COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_CFG_INTDIR}/simpleDraw${CMAKE_EXECUTABLE_SUFFIX}" "${CMAKE_CURRENT_SOURCE_DIR}/bin"
)